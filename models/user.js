// const Schema = require('mongoose');
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userScheme = new Schema({
  name: {
      type: String,
      default: "NoName",
      required: true,
      minlength: 2,
      maxlength: 35
    },
    age: {
      type: Number,
      default: 18,
      required: true,
      min: 1,
      max:130
    },
    status: {
      type: String,
      default: "User"
    }
  },{ versionKey: false });

const User = mongoose.model("User", userScheme);

module.exports = User;
