// const Schema = require('mongoose');
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const adminScheme = new Schema({
  name: {
      type: String,
      default: "NoName",
      required: true,
      minlength: 2,
      maxlength: 35
    },
    age: {
      type: Number,
      default: 18,
      required: true,
      min: 1,
      max:130
    },
    status: {
      type: String,
      default: "Admin"
    }
  },{ versionKey: false });

const Admin = mongoose.model("Admin", adminScheme);

module.exports = Admin;
