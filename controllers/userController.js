const User = require('../models/user')
const mongoose = require("mongoose");

const addUser = function (req, res) {
  User.create({name: req.body.name, age: req.body.age}, function(err, doc){
    if(err) return console.log(err);
    res.send(`Сохранен объект ${doc}`);
  });
};

const getUsers = function (req, res) {
  User.find({_id: req.params.id}, function(err, doc){
  if(err) return console.log(err);
    res.send(`Данные пользователя: ${doc}`);
  })
};

const deleteUser = (req, res) => {
  User.findOneAndDelete({_id: req.params.id}, function(err, doc){
    if(err) return console.log(err);
      res.send(`Пользователь ${doc} удален.`);
    })
};

const putUser = function (req, res) {
  User.findByIdAndUpdate({_id: req.params.id}, {name: req.body.name, age: req.body.age}, function(err, user){
    if(err) return console.log(err);
      res.send(`Данные пользователя ${user} обновлены.`);
    })
};

module.exports = { deleteUser, putUser, getUsers, addUser };
