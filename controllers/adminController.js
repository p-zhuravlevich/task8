const Admin = require('../models/admin')
const mongoose = require("mongoose");

const addAdmin = function (req, res) {
  Admin.create({name: req.body.name, age: req.body.age}, function(err, doc){
    if(err) return console.log(err);
    res.send(`Сохранен объект ${doc}`);
  });
};

const getAdmins = function (req, res) {
  Admin.find({_id: req.params.id}, function(err, doc){
  if(err) return console.log(err);
    res.send(`Данные пользователя: ${doc}`);
  })
};

const deleteAdmin = (req, res) => {
  Admin.findOneAndDelete({_id: req.params.id}, function(err, doc){
    if(err) return console.log(err);
      res.send(`Пользователь удален.`);
    })
};

const putAdmin = function (req, res) {
  Admin.findByIdAndUpdate({_id: req.params.id}, {name: req.body.name, age: req.body.age}, function(err, user){
    if(err) return console.log(err);
      res.send(`Данные пользователя ${user} обновлены.`);
    })
};

module.exports = { deleteAdmin, putAdmin, getAdmins, addAdmin };
